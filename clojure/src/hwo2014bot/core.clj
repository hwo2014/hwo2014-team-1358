(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(declare colour race)

(def switching (atom false))

(defn our-car [msg]
  (first (filter #(= (-> % :id :color) colour) (msg :data))))

(defn bend [piece]
  (let [angle (piece :angle)]
    (when angle
      (if (pos? angle) :right :left))))

(defn switch [pieceIndex]
  (reset! switching true)
  (println "Switching")
  {:msgType "switchLane"
   :data (->> (drop (inc pieceIndex) (cycle (-> race :track :pieces)))
              (map bend)
              (drop-while nil?)
              (first)
              {:left "Left"
               :right "Right"})})

(defn switch? [pieceIndex]
  (not (nil? (:switch (nth (-> race :track :pieces)
               pieceIndex)))))

(def ping 
  {:msgType "ping" :data "ping"})

(defmulti handle-msg :msgType)

(defmethod handle-msg "yourCar" [msg]
  (def colour (-> msg :data :color))
  ping)

(defmethod handle-msg "gameInit" [msg]
  (def race (-> msg :data :race))
  ping)

(defmethod handle-msg "carPositions" [msg]
  (if (and (not @switching)
           (not (switch? (-> (our-car msg) :piecePosition :pieceIndex))))
      (switch (-> (our-car msg) :piecePosition :pieceIndex))
      (do 
          (when (switch? (-> (our-car msg) :piecePosition :pieceIndex))
                (reset! switching false))
          {:msgType "throttle" :data 0.5})))

(defmethod handle-msg :default [msg]
  ping)

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "yourCar" (println (str "We are: " (-> msg :data :color)))
    "gameInit" (println "Race initialized")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (send-message channel (handle-msg msg))
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
